package geektrust.Entities;

import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Emblem Test")
public class EmblemTest {
    
    @Test
    @DisplayName("Should get CharacterMap of Emblem without exposing it")
    public void Test1() {
        HashMap<Character, Integer> expected = new HashMap<Character, Integer>() {
            {
                put('a', 3);
                put('b', 1);
                put('z', 2);
            }
        };
        HashMap<Character, Integer> actual = new Emblem("zaBaZa").getEmblemCharMap();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should get length of Emblem without exposing it")
    public void Test2() {
        Integer expected = 6;
        Integer actual = new Emblem("zaBaZa").getEmblemLength();
        Assertions.assertEquals(expected, actual);
    }

}
