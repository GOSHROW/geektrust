package geektrust.Entities;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import geektrust.Exceptions.MessageNotAlpha;

@DisplayName("Kingdom Test")
public class KingdomTest {
    
    @Test
    @DisplayName("Should Properly Check if the Message contains the encrypted Emblem")
    public void Test1() throws MessageNotAlpha {
        ArrayList<Boolean> expected = new ArrayList<Boolean>(Arrays.asList(true, false, false));
        ArrayList<Boolean> actual = new ArrayList<>();
        Kingdom kingdom = new Kingdom("delhi", "tiger");
        for (String message : new ArrayList<String>(Arrays.asList("ynsljswa", "nl", "abcabd"))) {
            kingdom.checkMessageForEmblem(message);
            actual.add(kingdom.isAllied());
        }
        Assertions.assertEquals(expected, actual);
    }
}
