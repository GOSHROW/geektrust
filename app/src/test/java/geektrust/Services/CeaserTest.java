package geektrust.Services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import geektrust.Exceptions.MessageNotAlpha;

@DisplayName("Ceaser Test")
public class CeaserTest {
    
    @Test
    @DisplayName("Decrypted String must align to the Proper String")
    public void test1() throws MessageNotAlpha {
        Ceaser ceaser = new Ceaser();
        String encrypted = "awsefyh";
        Integer shift = 59;
        String expected = "tplxyra";
        String actual = ceaser.decrypt(encrypted, shift);
        Assertions.assertEquals(expected, actual);
    }
}
