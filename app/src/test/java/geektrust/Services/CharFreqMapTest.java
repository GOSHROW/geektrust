package geektrust.Services;

import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("CharFreqMap Test")
public class CharFreqMapTest {
    
    @Test
    @DisplayName("Should get Frequency for all characters in a HashMap")
    public void Test1() {
        String str = "MccartneyLennon";
        HashMap<Character, Integer> expected = new HashMap<Character, Integer>() {
            {
                put('M', 1);
                put('c', 2);
                put('a', 1);
                put('r', 1);
                put('t', 1);
                put('n', 4);
                put('e', 2);
                put('y', 1);
                put('L', 1);
                put('o', 1);
            }
        };
        HashMap<Character, Integer> actual = new CharFreqMap().getCharFreqMap(str);

        Assertions.assertEquals(expected, actual);
    }
}
