package geektrust.Services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import geektrust.Entities.Kingdom;
import geektrust.Exceptions.KingdomNameNotFound;
import geektrust.Exceptions.MessageNotAlpha;

@DisplayName("CheckMessages Test")
public class CheckMessagesTest {

    @Test
    @DisplayName("Should check if the Messages correspond to the emblem of the Kingdom")
    public void Test1() throws MessageNotAlpha, KingdomNameNotFound {
        HashMap<String, Kingdom> kingdoms = new HashMap<String, Kingdom>() {
            {
                put("sparta", new Kingdom("sparta", "tiger"));
                put("spartb", new Kingdom("spartb", "liger"));
                put("spartc", new Kingdom("spartc", "eiger"));
            }
        };
        HashMap<String, String> messageMap = new HashMap<String, String>() {
            {
                put("sparta", "ynsljswa");
                put("spartb", "nl");
                put("spartc", "abacd");
            }
        };

        new CheckMessages(kingdoms, messageMap);
        ArrayList<Boolean> expected = new ArrayList<Boolean>(Arrays.asList(true, false, false));
        ArrayList<Boolean> actual = new ArrayList<>();
        for (String kingdomName : new ArrayList<String>(Arrays.asList("sparta", "spartb", "spartc"))) {
            actual.add(kingdoms.get(kingdomName).isAllied());
        }
        Assertions.assertEquals(expected, actual);
    }

}
