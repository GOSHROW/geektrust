package geektrust.Services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("ContianmentCheck Test")
public class ContainmentCheckTest {
    
    @Test
    @DisplayName("Should check if the Message contains required Character Map")
    public void Test1() {
        ArrayList<Boolean> expected = new ArrayList<Boolean>(Arrays.asList(true, false, false));
        ArrayList<Boolean> actual = new ArrayList<>();

        ContainmentCheck containmentCheck = new ContainmentCheck();

        HashMap<Character, Integer> toBeContained = new HashMap<Character, Integer>() {
            {
                put('a', 3);
                put('b', 1);
                put('z', 2);
            }
        };

        for (String toContain : new ArrayList<String>(Arrays.asList("aaabzza", "abz", "aa"))) {
            actual.add(containmentCheck.containsCharMap(toContain, toBeContained));
        }
        
        Assertions.assertEquals(expected, actual);
    }
}
