package geektrust.Entities;

import java.util.HashMap;

import geektrust.Exceptions.*;
import geektrust.Services.*;

public class Kingdom {
    private String kingdomName;
    private Emblem emblem;
    private Boolean allegianceState;

    public Kingdom(String kingdomName, String emblem) {
        this.kingdomName = kingdomName;
        this.emblem = new Emblem(emblem);
        allegianceState = false;
    }

    private HashMap<Character, Integer> getEmblemCharMap() {
        return emblem.getEmblemCharMap();
    }
    
    private Integer getEmblemLength() {
        return emblem.getEmblemLength();
    }

    private String getDecryptedString(String message) throws MessageNotAlpha {
        Ceaser ceaser = new Ceaser();
        String decrypted = ceaser.decrypt(message, getEmblemLength());
        return decrypted;
    }

    public void checkMessageForEmblem(String message) throws MessageNotAlpha {
        String decrypted = getDecryptedString(message);
        HashMap<Character, Integer> emblemCharMap = getEmblemCharMap();

        allegianceState = new ContainmentCheck().containsCharMap(decrypted, emblemCharMap);
    }
    
    public Boolean isAllied() {
        return allegianceState;
    }

    @Override
    public String toString() {
        return kingdomName;
    }
}