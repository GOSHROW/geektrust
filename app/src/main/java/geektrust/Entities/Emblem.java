package geektrust.Entities;

import geektrust.Services.*;
import java.util.HashMap;

public class Emblem {
    private String emblem;

    public Emblem(String emblem) {
        this.emblem = emblem.toLowerCase();
    }

    // Creates a Frequency Counter Map for given Emblem string and exports.
    public HashMap<Character, Integer> getEmblemCharMap() {
        CharFreqMap charFreqMap = new CharFreqMap();
        return charFreqMap.getCharFreqMap(emblem);
    }

    // Get Length of Emblem string without Exposing the pvt variable.
    public Integer getEmblemLength() {
        return emblem.length();
    }
}
