package geektrust.Exceptions;

public class InputFileMalformed extends Exception{
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Given file does not ascribe to the provided format of input";
    }
}
