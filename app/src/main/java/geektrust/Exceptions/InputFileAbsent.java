package geektrust.Exceptions;

public class InputFileAbsent extends Exception{
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Given file is either not readable or missing";
    }
}