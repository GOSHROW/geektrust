package geektrust.Exceptions;

public class KingdomNameNotFound extends Exception {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Provided Kingdom Name has not been added in during initialization";
    }
}