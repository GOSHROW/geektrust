package geektrust.Exceptions;

public class MessageNotAlpha extends Exception{
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Message has unresolved Characters that are not neccessarily alphabetic";
    }
}
