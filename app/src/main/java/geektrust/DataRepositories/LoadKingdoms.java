package geektrust.DataRepositories;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import geektrust.Entities.*;

public class LoadKingdoms {
    private Set<Kingdom> kingdoms;

    public LoadKingdoms() {
        kingdoms = new HashSet<>();

        // Hard-coding the values since no data loading related commands provided
        // Helps eliminate third party parser packages
        kingdoms.add(new Kingdom("SPACE", "Gorilla"));
        kingdoms.add(new Kingdom("LAND", "Panda"));
        kingdoms.add(new Kingdom("WATER", "Octopus"));
        kingdoms.add(new Kingdom("ICE", "Mammoth"));
        kingdoms.add(new Kingdom("AIR", "Owl"));
        kingdoms.add(new Kingdom("FIRE", "Dragon"));
    }

    public HashMap<String, Kingdom> getKingdoms() {
        HashMap<String, Kingdom> retKingdomsMap = new HashMap<>();
        for (Kingdom kingdom : kingdoms) {
            retKingdomsMap.put(kingdom.toString().toLowerCase(), kingdom);
        }
        return retKingdomsMap;
    }
}
