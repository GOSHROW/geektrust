package geektrust.DataRepositories;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import geektrust.Exceptions.*;

public class LoadMessages {
    private HashMap<String, String> messageMap;
    private ArrayList<String> orderKingdom;

    private String makeMessageAlpha(String message) {
        StringBuffer retStringBuffer = new StringBuffer();
        for (Character ch : message.toCharArray()) {
            if (Character.isLetter(ch)) {
                retStringBuffer.append(ch);
            }
        }
        return retStringBuffer.toString();
    }

    private String removeKingdomGetMessage(List<String> strArray) {
        String[] retArray = new String[strArray.size() - 1];
        for (int i = 1; i < strArray.size(); ++i) {
            retArray[i - 1] = strArray.get(i);
        }
        String retMessage = String.join("", retArray);
        return makeMessageAlpha(retMessage);
    }

    public LoadMessages(String pathMessageFile) throws FileNotFoundException, InputFileMalformed {
        File messageFile = new File(pathMessageFile);
        messageMap = new HashMap<>();
        orderKingdom = new ArrayList<>();

        if (!messageFile.exists()) {
            System.out.println(pathMessageFile + " cannot be located");
            throw new FileNotFoundException();
        }

        try {
            Scanner sc = new Scanner(messageFile);
            String kingdomName, message = " ";
            while (sc.hasNextLine()) {
                List<String> line = Arrays.asList(sc.nextLine().split("\\s+"));
                kingdomName = line.get(0);
                message = removeKingdomGetMessage(line);
                messageMap.put(kingdomName, message);
                orderKingdom.add(kingdomName);
            }
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new InputFileMalformed();
        }
    }

    public HashMap<String, String> getMessages() {
        return messageMap;
    }

    public ArrayList<String> getOrderKingdom() {
        return orderKingdom;
    }

}
