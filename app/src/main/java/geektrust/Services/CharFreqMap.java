package geektrust.Services;

import java.util.HashMap;

public class CharFreqMap {
    
    public HashMap<Character, Integer> getCharFreqMap(String str) {
        HashMap<Character, Integer> retCharMap = new HashMap<>();
        for (Character ch : str.toCharArray()) {
            retCharMap.compute(ch, (k, v) -> (v == null ? 0 : v) + 1);
        }
        return retCharMap;
    }
}
