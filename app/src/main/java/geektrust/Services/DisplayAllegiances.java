package geektrust.Services;

import java.util.ArrayList;
import java.util.HashMap;

import geektrust.Entities.*;

public class DisplayAllegiances {
    public DisplayAllegiances(HashMap<String, Kingdom> kingdoms, ArrayList<String> orderKingdom) {
        // HardCoded as per the problem Statement
        Integer requiredAllegiances = 3;
        String messageSender = "SPACE";
        String defaultMessage = "NONE";

        ArrayList<String> allies = new ArrayList<>();
        for (Kingdom kingdom : kingdoms.values()) {
            if (kingdom.isAllied() && ! kingdom.toString().equalsIgnoreCase(messageSender)) {
                requiredAllegiances -= 1;
                allies.add(kingdom.toString());
            }
        }

        if (requiredAllegiances <= 0) {
            StringBuffer alliesToPrint = new StringBuffer();
            for (String ally : orderKingdom) {
                if (allies.contains(ally)) {
                    alliesToPrint.append(" " + ally);
                }
            }
            System.out.println(messageSender + alliesToPrint.toString());
        } else {
            System.out.println(defaultMessage);
        }
    }
}
