package geektrust.Services;

import geektrust.Exceptions.*;
import java.util.HashMap;
import java.util.Set;

public class Ceaser {
    private HashMap<Character, Character> cipherMap;

    public Ceaser() {
        cipherMap = new HashMap<>();
        for (Character alphabet = 'a'; alphabet <= 'z'; alphabet++) {
            cipherMap.put(alphabet, alphabet);
        }
    }
    
    private void shiftCipherMap(Integer shift) {
        Set<Character> keySet = cipherMap.keySet();
        shift %= 26;

        for (Character key : keySet) {
            Character prevMapping = cipherMap.get(key);
            char newMapping;
            if (prevMapping - shift >= 'a') {
                newMapping = (char) (prevMapping - shift);
            } else {
                newMapping = (char) (prevMapping + 26 - shift);
            }
            cipherMap.put(key, newMapping);
        }
    }

    private char[] processMessage(String message) throws MessageNotAlpha {
        if (message == null || message == "") {
            return message.toCharArray();
        }
        
        message = message.toLowerCase();
        char[] messageCharArray = message.toCharArray();
        
        for (Character ch: messageCharArray) {
            if (!Character.isLetter(ch)) {
                throw new MessageNotAlpha();
            }
        }
        
        return messageCharArray;
    }

    public String decrypt(String message, Integer shift) throws MessageNotAlpha {
        char[] messageCharArray = processMessage(message);
        shiftCipherMap(shift);
        StringBuffer retBuffer = new StringBuffer(message.length());
        
        for (Character ch : messageCharArray) {
            retBuffer.append(cipherMap.get(ch));
        }
        return retBuffer.toString();
    }
}