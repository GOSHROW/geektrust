package geektrust.Services;

import java.util.HashMap;
import java.util.Set;

public class ContainmentCheck {
    
    public boolean containsCharMap(String toContain, HashMap<Character, Integer> toBeContained) {
        
        toContain = toContain.toLowerCase();
        HashMap<Character, Integer> charMapOfStr = new CharFreqMap().getCharFreqMap(toContain);
        Set<Character> keySet = toBeContained.keySet();

        for (Character key : keySet) {
            Integer expectedFrequency = toBeContained.get(key);
            Integer actualFrequency = charMapOfStr.containsKey(key) ? charMapOfStr.get(key) : 0;
            if (expectedFrequency > actualFrequency) {
                return false;
            }
        }
        
        return true;
    }
}
