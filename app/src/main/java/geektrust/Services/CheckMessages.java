package geektrust.Services;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import geektrust.Entities.*;
import geektrust.Exceptions.*;

public class CheckMessages {

    public CheckMessages(HashMap<String, Kingdom> kingdoms, HashMap<String, String> messageMap)
            throws MessageNotAlpha, KingdomNameNotFound {
        Set<Map.Entry<String, String>> entrySet = messageMap.entrySet();

        for (Map.Entry<String, String> entry : entrySet) {
            String kingdomName = entry.getKey().toLowerCase(), message = entry.getValue();
            if (!kingdoms.containsKey(kingdomName)) {
                throw new KingdomNameNotFound();
            }
            Kingdom kingdom = kingdoms.get(kingdomName);
            kingdom.checkMessageForEmblem(message);
        }
    }
}
