# GeekTrust Challenge
## Tame of Throne
## Swapnil Ghosh
## 31st July 2021

#### Navigating through File Structure

The implementaion follows OOP principles such that the Entities, Data Repositories and Services required as per the document have been identified, segregated and maintained with just their behaviours and attributes. Corresponding Tests have been added wherever required.

Source Code may be found in ```app/src/main/java/geektrust``` . Unit Tests at ```app/src/test/java/geektrust``` .

#### Setting Up

```app/build.gradle``` can be reffered for any concerns regarding SetUp. Basically the implementation requires *Gradle* alongwith *Junit Jupiter* which adds up as a dependancy.

To build the jar file, use 
```gradle build```

Use it as 
```java -jar app/build/libs/geektrust.jar <path-to-input-file>```

For ease of automated testing, I have cloned the jar file to parent directory.

#### Assumption in the Use Case

* The Output order for a case of allegiance is irrespective 1st index onwards.
* Throw custom Exceptions on undefined nature of Input.